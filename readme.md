# TCS new architecture demo

Architecture Diagram can be found here https://app.diagrams.net/#G18ShJDaR8ilFxXLX8un24DnlB60E-NIkR

## How to run

```sh
cp .env.example .env
cp config.json.example config.json
```

```sh
docker-compose up
```

## Example api
- use `postman` or any `http client` to test the api

### using postman
-  download postman example collection [here](https://nc.narit.or.th/nextcloud/index.php/s/ej7dsjP3AjkfZnn)
- try to run `POST add scan`, `POST run-queue`

### using other http client
`POST add-scan`
- method `POST`
- url `/observation`
- body
```json
{
    "observation": {
        "proj_id": "demo-01",
        "obs_id": "demo-obs-01"
    },
    "receiver": {
        "band": "test"
    }
}
```

`POST run-queue`
- method `POST`
- url `/observation/run-queue`





see docker logging terminal showing that we can connect to the mock frontend katcp 

```
obs_service            | [<src.models.Scan.Scan object at 0x7f4815457910>]
obs_service            | 
obs_service            | 127.0.0.1 - - [26/Jul/2023 08:26:54] "POST /observation HTTP/1.1" 200 -
obs_service            | Scan: run
obs_service            | Frontend: connect
obs_service            | Frontend: configure
obs_service            | Frontend: set_digitizer
obs_service            | [b'status', b'1']
obs_service            | Frontend: set_attenuation
obs_service            | [b'value', b'31.5']
obs_service            | Scan: run_subscans
obs_service            | start data pipeline
obs_service            | backend measurement prepare
obs_service            | backend measurement start
obs_service            | Frontend: disconnect
obs_service            | 
obs_service            | 127.0.0.1 - - [26/Jul/2023 08:26:59] "POST /observation/run-queue HTTP/1.1" 200 -
```

## Testing Notification Channel

- when run `POST add scan` the notification supplier is already publishing string `mock metadata`. To test if it's working you can run the standalone consumer 

```sh
cd src/utlis/NotificationChannel/src
```
```sh
python Consumer.py
```

You should see message `mock metadata` in the terminal logging 

```
channel: channel_scan_metadata
connect to supplier at 192.168.44.230:55000
Waiting for cancel...
mock metadata
Waiting for cancel...
mock metadata
```


