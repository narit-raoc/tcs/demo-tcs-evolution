from flask import Flask, make_response, request, jsonify
import json
from src.models.Observation import Observation
from src.models.Scan import Scan
import sys
sys.stdout.flush()

app = Flask(__name__)
cache = {
    'observations': Observation(),
}

@app.route('/observation', methods=['POST'])
def add_scan():    
    try:
        body = json.loads(request.data)
        scan = Scan(body)
        cache['observations'].add_scan(scan)
        print('', flush=True)
        return make_response(jsonify(len(cache['observations'].queue)), 200)
    except ValueError as error:
        return make_response(error, 400)
    except Exception as error:
        return make_response(error, 500)
    
@app.route('/observation/run-queue', methods=['POST'])
async def run_queue():
    try:
        await cache['observations'].run_queue()
        print('', flush=True)
        return make_response(jsonify('ok'), 200)
    except ValueError as error:
        return make_response(error, 400)
    except Exception as error:
        return make_response(error, 500)

@app.route('/observation/queue', methods=['GET'])
def get_queue():
    try:
        return make_response(jsonify(len(cache['observations'].queue)), 200)
    except ValueError as error:
        return make_response(error, 400)
    except Exception as error:
        return make_response(error, 500)
    

    

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
    