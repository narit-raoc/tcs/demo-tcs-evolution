import aiokatcp
import asyncio

class MyServer(aiokatcp.DeviceServer):
    VERSION = 'myserver-api-1.0'
    BUILD_STATE = 'myserver-1.0.1.dev0'

    def __init__(self, host, port):
        super().__init__(host, port)

        sensors = [
            aiokatcp.Sensor(int, 'counter-queries', 'number times ?counter was called', default=0, initial_status=aiokatcp.Sensor.Status.NOMINAL),
            aiokatcp.Sensor(int, 'eiei', 'number times ?counter was called', default=0, initial_status=aiokatcp.Sensor.Status.NOMINAL)
        ]

        for sensor in sensors:
            self.sensors.add(sensor)

    async def request_set_digitizer(self, ctx, status: bool):
        """Take a person's name and greet them"""
        return 'status', status
    
    async def request_set_attenuation(self, ctx, value: float):
        """Take a person's name and greet them"""
        print('set_attenuation')
        return 'value', value

async def main():
    server = MyServer('127.0.0.1', 6000)
    await server.start()
    await server.join()

if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
    asyncio.get_event_loop().close()