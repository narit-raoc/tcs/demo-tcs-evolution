from dotenv import load_dotenv
import os

load_dotenv()

HOST = os.getenv('CDB_HOST')
PORT = int(os.getenv('CDB_PORT'))