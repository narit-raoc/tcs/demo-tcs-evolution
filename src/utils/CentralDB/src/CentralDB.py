import logging
import redis
import src.utils.CentralDB.src.cdbConfig as config

class CentralDB:
    def __init__(self):
        #Logger for this CentralDB
        self.logger = logging.getLogger("CentralDB")

        # connect to db
        self.client = redis.Redis(
            host=config.HOST, 
            port=config.PORT,
        )
        self.cache = redis.StrictRedis()
    
    def __del__(self):
        pass
    
    def get_nc_port(self, channel):
        db_key = 'nc_{}'.format(channel)
        nc_port = int(self.client.get(db_key))
        return nc_port
    
    def set_nc_port(self, channel, port):
        db_key = 'nc_{}'.format(channel)
        self.client.set(db_key, port)

    def get_all_nc_port(self):
        return self.cache.scan_iter('nc_*')

    def get(self, key, dtype=None):
        try:
            value = self.client.get(key).decode('utf-8')
            if dtype != None:
                return value and dtype(value)
            return value
        except AttributeError:
            return None
    
    def set(self, key, value):
        self.client.set(key, str(value))

    def reset_subscan(self):
        self.reset_pattern('SUBS_*')

    def reset_obs(self):
        # reset all redis key except keys for notification channel
        # this function will be used to clean up before starting new scan
        self.reset_pattern('[^nc_]*')

    def reset_pattern(self, pattern):
        keys = self.client.keys(pattern)
        if len(keys) > 0:
            self.client.delete(*keys)



if __name__ == "__main__":
    try:
        db = CentralDB()
        
        db.set_nc_port('test_nc_port', 1234)
        print(db.get_nc_port('test_nc_port'))

        db.set('test_key', 'test_value')
        print(db.get('test_key'))
    except KeyboardInterrupt:
        print('KeyboardInterrupt')
    