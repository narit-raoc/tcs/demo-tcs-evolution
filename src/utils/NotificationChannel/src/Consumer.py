import logging
import threading
import time
import zmq
import pickle
import json
import os
import socket
from CentralDB import CentralDB

class Consumer:
    def __init__(self, channel, ip_supplier='127.0.0.1'):
        """
        Constructor.

        Params:
        - channelName is the channel name
        - ip_supplier is ip of the supplier (optional)

        Returns: Nothing
        """
        self.channel = channel

        # Logger for this consumer
        self.logger = logging.getLogger("Consumer {}".format(self.channel))

        self.cdb = CentralDB()
        
        self.ip_supplier = ip_supplier
        
        try:
            # get nc port from central db
            self.port_supplier = self.cdb.get_nc_port(self.channel)
        except TypeError:
            # no port for this chennel
            # get new available port and save to cdb
            self.port_supplier = self.get_available_port(host=self.ip_supplier)
            self.cdb.set_nc_port(self.channel, self.port_supplier)

        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.SUB)
        self.socket.connect("tcp://{}:{}".format(self.ip_supplier, 55000))

        self.logger.info('connect to supplier at {}:{}'.format(self.ip_supplier, self.port_supplier))

        self.consumer_active = threading.Event()
    
    def get_available_port(self, host='127.0.0.1', port=50000, max_port=65535):
        return 55000
        # sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # all_channel = self.cdb.get_all_nc_port()
        # reserved_port = []

        # for key in all_channel:
        #     try:
        #         p = self.cdb.get(key.decode('utf-8'))
        #         reserved_port.append(int(p))
        #     except Exception as e:
        #         print(e)

        # if len(reserved_port) > 0:
        #     max_reserved_port = max(reserved_port)
        #     port = max_reserved_port + 1
            
        # while port <= max_port:
        #     try:
        #         print('port', port)
        #         sock.bind((host, port))
        #         sock.close()
        #         return port
        #     except OSError:
        #         port += 1
        # raise IOError('no free ports')

    def add_subscription(self, handler):
        self.socket.setsockopt_string(zmq.SUBSCRIBE, self.channel)
        self.socket.setsockopt(zmq.RCVTIMEO, 5000)
        self.logger.info("connecting...")
        self.consumer_active.set()

        consumer_thread = threading.Thread(target=self.connect, args=[handler])
        consumer_thread.start()

        self.logger.info("connected")

    def connect(self, handler):
        while self.consumer_active.is_set():
            try:
                [channel_from_message, message] = self.socket.recv_multipart()
                unpickled_message = pickle.loads(message)
                handler(unpickled_message)
            except zmq.ZMQError as e:
                if e.errno == zmq.EAGAIN:
                    pass # no message was ready (yet!)
        
        # if exit while loop means consumer already disconnected
        self.logger.info("disconnected".format(self.channel))
        self.socket.close()
        self.context.destroy()

    def disconnect(self):
        self.logger.info("disconnecting...".format(self.channel))
        self.consumer_active.clear()


if __name__ == "__main__":
    try:
        consumer = Consumer("channel_scan_metadata",'127.0.0.1') ## change ip here to test
        print('channel: {}'.format(consumer.channel))
        print('connect to supplier at {}:{}'.format(consumer.ip_supplier, consumer.port_supplier))

        def test_handler(data):
            print(data)

        def unblock_condition_function():
            return(consumer.consumer_active.is_set() == False)

        consumer.add_subscription(test_handler)
        ready_to_unblock = threading.Condition()
        ready_to_unblock.acquire()

        while not ready_to_unblock.wait_for(unblock_condition_function, timeout=3):
            print("Waiting for cancel...")

    except KeyboardInterrupt:
        consumer.disconnect()
