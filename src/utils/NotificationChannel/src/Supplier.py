import logging
import zmq
import time
import pickle
import socket
import src.utils.NotificationChannel.src.config as config
from src.utils.CentralDB.src.CentralDB import CentralDB

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
        
class ZMQSingleton(metaclass=Singleton):
    def __init__(self, port):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.PUB)
        self.socket.bind('tcp://*:%s' % port)

    def __del__(self):
        self.socket.close()
        self.context.destroy()

class Supplier:
    
    def __init__(self, channel):
        '''
        Constructor.

        Params:
        - channel is the channel name
        '''
        self.channel = channel
        self.port = config.NC_PORT
        self.zmq = ZMQSingleton(self.port)

        # Store port in cdb because now tcs read nc port from cdb
        # TODO: remove after all tcs consumer don't read nc port from cdb 
        self.cdb = CentralDB()
        self.cdb.set_nc_port(self.channel, self.port)

        # Logger for this supplier
        self.logger = logging.getLogger("Supplier {}".format(channel))

    def disconnect(self):
        del self.zmq

    def publish_event(self, message):
        pickled_message = pickle.dumps(message)
        self.zmq.socket.send_multipart([bytes(self.channel, 'utf-8'), pickled_message])

if __name__ == "__main__":
    try:
        channel = 'AntennaStatusChannel'
        supplier = Supplier(channel)
        print('channel: {}'.format(supplier.channel))
        print('port: {}'.format(supplier.port))
        for i in range(100):
            messagedata = 'message data {}'.format(i)
            print(messagedata)
            supplier.publish_event(messagedata)
            time.sleep(1)
    except KeyboardInterrupt:
        print('KeyboardInterrupt')