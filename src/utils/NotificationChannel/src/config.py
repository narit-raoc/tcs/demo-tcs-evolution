from dotenv import load_dotenv
import os

load_dotenv()

NC_PORT = os.getenv('NC_PORT')
