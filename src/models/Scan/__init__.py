from src.models.Frontend import Frontend
from src.utils.NotificationChannel.src.Supplier import Supplier
import constants
import threading

class Scan:
    def __init__(self, params):
        self.proj_id = params['observation']['proj_id']
        self.obs_id = params['observation']['obs_id']

        self.frontend = Frontend(params['receiver']['band'])

        self.metadata_supplier = Supplier(constants.NC_CHANNEL['metadata'])
        self.event_stop_stream_metadata = threading.Event()
        metadata_stream_thread = threading.Thread(target=self.stream_metadata_worker)
        metadata_stream_thread.start()
    
    def __del__(self):
        self.event_stop_stream_metadata.set()

    async def connect_device(self):
        await self.frontend.connect()

    async def disconnect_device(self):
        await self.frontend.disconnect()

    async def run(self):
        print('Scan: run')
        await self.connect_device()
        await self.frontend.configure()
        await self.run_subscans()


    async def run_subscans(self):
        print('Scan: run_subscans')
        print('start data pipeline')
        print('backend measurement prepare')
        print('backend measurement start')
        await self.disconnect_device()


    def stop(self):
        print('Scan: stop')

    def update_metadata(self):
        print('Scan: update_metadata')
        self.metadata_supplier.publish_event('mock metadata')

    def stream_metadata_worker(self, update_period=5):
        self.event_stop_stream_metadata.clear()
        while not self.event_stop_stream_metadata.is_set():
            self.metadata_supplier.publish_event('mock metadata')
            self.event_stop_stream_metadata.wait(timeout=update_period)
