from src.utils.NotificationChannel.src.Supplier import Supplier
from src.models.Frontend import Frontend

class Observation:
    def __init__(self):
        self.queue = []
        self.results = {}
    
    def __del__(self):
        pass

    def add_scan(self, scan):
        self.queue.append(scan)
        print(self.queue)
    
    def clear_queue(self):
        self.queue.clear()

    async def run_queue(self):
        while len(self.queue) > 0:
            current_scan = self.queue.pop(0)
            await current_scan.run()

    def stop_queue(self):
        print('stop_queue')