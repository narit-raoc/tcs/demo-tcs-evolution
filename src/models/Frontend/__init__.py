import config
import aiokatcp

class Frontend:
    def __init__(self, receiver_type, attenuation = 31.5):
        self.receiver_type = receiver_type
        self.attenuation = attenuation

        self.host = config.RECEIVER[receiver_type]['host']
        self.port = config.RECEIVER[receiver_type]['port']
        self.method = config.RECEIVER[receiver_type]['method']

        self.client = None
    
    def __del__(self):
        pass

    async def connect(self):
        print('Frontend: connect')
        self.client = await aiokatcp.Client.connect(self.host, self.port)

    async def disconnect(self):
        print('Frontend: disconnect')
        self.client.close()
        await self.client.wait_closed()
        await self.client.wait_disconnected()

    async def configure(self):
        print('Frontend: configure')
        await self.set_digitizer(True)
        await self.set_attenuation(self.attenuation)

    async def set_digitizer(self, value):
        print('Frontend: set_digitizer')
        reply, informs = await self.client.request('set-digitizer', value)
        print(reply)
        for inform in informs:
            print(b' '.join(inform.arguments).decode('ascii'))

    async def set_attenuation(self, value):
        print('Frontend: set_attenuation')
        reply, informs = await self.client.request('set-attenuation', value)
        print(reply)
        for inform in informs:
            print(b' '.join(inform.arguments).decode('ascii'))

    